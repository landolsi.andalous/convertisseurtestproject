import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConvertisseurComponent } from './convertisseur.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
const materialModules = [MatCheckboxModule, MatInputModule, MatFormFieldModule, MatInputModule, MatIconModule,]



@NgModule({
  declarations: [ConvertisseurComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ...materialModules,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    
  ],
  exports: [
    ConvertisseurComponent
  ]
})
export class ConvertisseurModule { }

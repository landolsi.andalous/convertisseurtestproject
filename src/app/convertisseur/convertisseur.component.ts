import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-convertisseur',
  templateUrl: './convertisseur.component.html',
  styleUrls: ['./convertisseur.component.sass']
})
export class ConvertisseurComponent implements OnInit {
  public timer : Subscription;
  public isChanged : boolean= false;
  public exchangeForm: FormGroup;
  public exchangeRate = 1.1;
  public reloadInterval = 3000;
  constructor() { }
  
  ngOnInit() {
    this.initForm();
    setTimeout (() => {
      
      this.timer = timer(0, this.reloadInterval).subscribe(
      () => {
        const max = 1.15;
        const min =  1.05;
        this.exchangeRate = this.getRandomArbitrary(min, max);
        const dolarvalue = this.exchangeRate * this.exchangeForm.controls['euro'].value;
        this.exchangeForm.controls['dolar'].setValue(dolarvalue);

      }
    )
   }, 3000);
  }

  initForm() {
    this.exchangeForm = new FormGroup({
      euro:  new FormControl(0),
      dolar: new FormControl({value : 0, disabled: true}),
      
    });
    this.exchangeForm.controls['euro'].valueChanges.subscribe(
      euroValue => {
        if (this.exchangeForm.controls['euro'].disabled) {
          let dolarValue = this.exchangeRate * euroValue
          this.exchangeForm.controls['dolar'].setValue(dolarValue);
        }
      }
    )
  
    
  }
  
  public getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  change() {
    
    if(this.isChanged == false) {
      this.exchangeForm.controls['dolar'].enable();
      this.exchangeForm.controls['euro'].disable()
    }
    else {
      this.exchangeForm.controls['dolar'].disable()
      this.exchangeForm.controls['euro'].enable();
    }
    if (this.timer != undefined) {
      this.timer.unsubscribe();

    }
    timer(0, this.reloadInterval).subscribe(
      () => {
        const max = 1.15;
        const min =  1.05;
        this.exchangeRate = this.getRandomArbitrary(min, max);
        const eurovalue =  this.exchangeForm.controls['dolar'].value * this.exchangeRate ;
        this.exchangeForm.controls['euro'].setValue(eurovalue);

      })
    this.isChanged = !this.isChanged;
  }
}
